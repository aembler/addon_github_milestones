<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<h3><?=t('Milestones')?></h3>

<?php if (count($milestones)) {

    foreach($milestones as $milestone) { ?>

    <div class="ccm-block-type-github-milestone">
        <h4><?=$milestone->getTitle()?></h4>

        <?php if ($milestone->getDescription()) { ?>
            <p><?=$milestone->getDescription()?></p>
        <?php } ?>

        <h5><?=t('Status')?></h5>
        <?=t('Open: %s, Closed: %s', $milestone->getOpenIssues(), $milestone->getClosedIssues())?>

        <div class="ccm-block-type-github-milestone-more-info">
            <a href="<?=$milestone->getPublicUrl()?>"><?=t('View Issues')?></a>
        </div>

    </div>

    <?php } ?>

<?php } else { ?>

    <p><?=t('There are no open milestones.')?></p>

<?php } ?>
